/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.lab.st1300;

/**
 *
 * @author tomye
 */
public class Homework02BubbleSort {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int size=100000;//es la cantidad de numeros que se ingresaran sucesivamente
        int i=0;
        
        int[]array = new int[size];
        
        for( i = 0; i<array.length; i++){
            array[i]= size;
            size--;
        }
        
        Sorting.bubbleSort(array);
        
        for( i = 0; i<array.length; i++){
            System.out.println(array[i]);
            System.nanoTime();
        }
    }
}
